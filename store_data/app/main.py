"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import json
import os
from xpresso.ai.core.data.automl.structured_dataset import StructuredDataset
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
from xpresso.ai.client.controller_client import ControllerClient
from sqlalchemy import create_engine

__author__ = "### Author ###"

logger = XprLogger("store_data",level=logging.INFO)


class StoreData(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename, parameters_commit_id):
        super().__init__(name="store_data",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)

        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        """ Initialize all the required constansts and data her """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            if os.getenv("enable_local_execution", False) is False:
                status = {
                    "status": {"status": "Fetching data from repository"},
                }
                self.report_status(status)
                logger.info("Fetching data from repository")

                #login to the version manager
                os.makedirs("/root/.xpr/", exist_ok=True)
                os.system("echo -n {} > /root/.xpr/.workspace".format("default"))
                client = ControllerClient()
                client.login(self.run_parameters["xpresso_uid"], self.run_parameters["xpresso_pwd"])

                # Create instance of VersionControllerFactory
                version_controller_factory = VersionControllerFactory()

                # Get version_controller object
                version_controller = version_controller_factory.get_version_controller()

                # pull data
                repo_name = "sample_project_etl_bi"
                branch_name = self.run_parameters["branch_name"]
                commit_id = self.run_parameters["new_commit_id"]
                dataset = version_controller.pull_dataset(repo_name=repo_name, branch_name=branch_name,
                                                                type="data")[1]
                logger.info("Fetched data from repository")
                status = {
                    "status": {"status": "Fetched data from repository"},
                }
                self.report_status (status)
            else:
                dataset = StructuredDataset()
                logger.info("Created dataset")

                # specify import parameters
                config = {
                    "type": "FS",
                    "data_source": "local",
                    "path": self.run_parameters["output_data_path"]
                }

                # import data
                dataset.import_dataset(config)
                logger.info("Fetched data from file")
                status = {
                    "status": {"status": "Fetched data from file"},
                }
                self.report_status(status)

            # store data in database
            host = self.run_parameters["db_url"]
            db = self.run_parameters["db_name"]
            user = self.run_parameters["db_user"]
            pwd = self.run_parameters["db_pwd"]
            port = self.run_parameters["db_port"]
            con = self.create_connection (host, port, user, pwd, db)
            logger.info("Connected to database {}".format(host))

            self.upload_dataset (con, "participant_info", dataset)
            logger.info('Successfully loaded the table from transformed dataset.')
            status = {
                "status": {"status": "Stored transformed data in database"},
            }
            self.report_status(status)

            con.close()

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()

    def create_connection(self, host, port, user, password, db):
        """
        Creates engine that establishes connection with database component
        :return: SQL connection object
        """
        sqlEngine = create_engine(
            "mysql+pymysql://{user}:{pw}@{ip}:{port}/{db}".format(user=user, pw=password,
                                                                  ip=host, port=port,
                                                                 db=db))
        dbConnection = sqlEngine.connect()
        return dbConnection

    def upload_dataset(self, sql_connection, table_name, dataset):
        """
        Exports data from excel sheets into respective tables
        :param table_name: SQL table in which data needs to be exported
        :param sql_connection: SQL connection object
        :param dataset: dataset to be exported
        :return:
        """
        try:
            df = dataset.data
            d1 = df.dropna()
            d1.drop_duplicates(inplace=True)
            d1.to_sql(name=table_name, con=sql_connection, if_exists='replace', index=False)
        except Exception as err:
            raise err

if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    # this component has 3 parameters: run_name, name of JSON file containing input parameters and commit ID
    # in data repository where params are stored
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    data_prep = StoreData(run_name, params_filename, params_commit_id)
    data_prep.start(run_name=sys.argv[1])