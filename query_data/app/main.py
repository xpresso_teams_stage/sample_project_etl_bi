"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"


import json
import logging
from json import JSONDecodeError
from flask import Flask
from flask import request
import pymysql.cursors
import traceback

# Following import are required for Xpresso. Do not remove this.
from xpresso.ai.core.logging.xpr_log import XprLogger

config_file = 'config/dev.json'

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="query_data",
                   level=logging.INFO)


def create_app(parameters_file) -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    with open(parameters_file) as f:
        parameters = json.load(f)

    return flask_app, parameters


app, parameters = create_app("/data/params.json")

@app.route('/')
def hello_world():
    """
    Send response to Hello World
    """

    logger.info("Received request from {}".format(request.remote_addr))
    try:
        logger.info("Processing the request")
        cfg_fs = open(config_file, 'r')
        config = json.load(cfg_fs)
        project_name = config["project_name"]
        logger.info("Request Processing Done")
        logger.info("Sending Response to {}".format(request.remote_addr))
        return '<html><body><b>Hello World from {}!</b></body></html>'.format(
            project_name
        )
    except (FileNotFoundError, JSONDecodeError):
        logger.error("Request Processing Failed")
        logger.info("Sending Default Response")
        return '<html><body><b>Hello World!</b></body></html>'

def create_connection (host, port, user, pwd, db):
    print("Connecting to database {} on host {} at port {} using credentials for {}".format(db, host, port, user))
    connection = pymysql.connect(host=host,
                                 port=port,
                                 user=user,
                                 passwd=pwd,
                                 db=db)
    print ("Connection established")
    return connection

def query_database (connection, query):
    filter_specified = False
    group_specified = False
    groups = list()
    if query is not None:
        if "filter" in query.keys():
            filter_specified = True
        if "group" in query.keys() and len (query["group"]) > 0:
           group_specified = True
           groups = query["group"]

    # form query
    sql = "SELECT "
    if group_specified:
        for group in groups:
            sql = sql + group + ", "

    sql = sql + " sum(num_participants) from participant_info where "

    if filter_specified:
        # get filter
        filter = query["filter"]
        for key in filter:
            sql = sql + key + " = '" + filter[key] + "' AND "

    # dummy filter parameter for maintaining syntax
    sql = sql + "1=1 "

    if group_specified:
        sql = sql + " GROUP BY " + ",".join(groups)

    print(sql)

    with connection.cursor() as cursor:
        # query db
        cursor.execute(sql)
        result = cursor.fetchall()
        response = dict()
        response["results"] = list()
        for row in result:
            print(row)
            row_dict = dict()
            count = -1
            for count in range(len(groups)):
                row_dict[groups[count]] = row[count]
            row_dict["num_participants"] = row[count+1]
            response["results"].append(row_dict)
        response["outcome"] = "success"

        return response

@app.route('/get_results', methods=['POST'])
def get_results():
    """
    gets response to query on dwh
    :param query: json with attributes and values
    :return: number of participants corresponding to the filter
    """
    try:
        print("reading parameters")
        host = parameters["db_url"]
        print(host)
        db = parameters["db_name"]
        print(db)
        user = parameters["db_user"]
        print(user)
        pwd = parameters["db_pwd"]
        port = int(parameters["db_port"])
        print(port)
        con = None
        con = create_connection(host, port, user, pwd, db)
        logger.info("Connected to database {}".format(host))
        query=request.get_json()
        print(query)
        response = query_database(con, query)
    except Exception:
        traceback.print_exc()
        response = dict()
        response["outcome"] = "error: failure issuing query "
        response["results"] = -1
    finally:
        try:
            con.close()
        finally:
            pass
    return response


if __name__ == '__main__':
    app.run(host="0.0.0.0")