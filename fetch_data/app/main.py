"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.data.automl.structured_dataset import StructuredDataset
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
from xpresso.ai.client.controller_client import ControllerClient
import json
import os
from xpresso.ai.core.commons.exceptions.xpr_exceptions import CreateBranchException
__author__ = "### Author ###"

logger = XprLogger("fetch_data",level=logging.INFO)


class FetchData(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename, parameters_commit_id):

        super().__init__(name="fetch_data",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)

        """ Initialize all the required constansts and data her """
        # load parameters
        self.parameters_file = parameters_filename

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            status = {
                "status": {"status": "Fetching data"},
            }
            self.report_status(status)
            logger.info("Fetching data")

            # create dataset object
            dataset = StructuredDataset()
            logger.info("Created dataset")

            # specify import parameters
            config = {
                "type": "FS",
                "data_source": "local",
                "path": self.run_parameters["input_data_path"]
            }

            # import data
            dataset.import_dataset(config)
            logger.info("Imported data: No. of rows = {}".format(dataset.data.shape[0]))

            # Print the Pandas DataFrame
            print(dataset.data.head())

            status = {
                "status": {"status": "Fetched data"},
                "metric": {"Number of rows": dataset.data.shape[0]}
            }
            self.report_status(status)

            if os.getenv("enable_local_execution", False) is False:
                # version the data
                status = {
                    "status": {"status": "Versioning data"},
                }
                self.report_status(status)
                logger.info("Versioning data")

                #login to the version manager
                os.makedirs("/root/.xpr/", exist_ok=True)
                os.system("echo -n {} > /root/.xpr/.workspace".format("default"))
                client = ControllerClient()
                client.login(self.run_parameters["xpresso_uid"], self.run_parameters["xpresso_pwd"])
                logger.info("Logged in to Version Controller")

                # Create instance of VersionControllerFactory
                version_controller_factory = VersionControllerFactory()

                # Get version_controller object
                version_controller = version_controller_factory.get_version_controller()

                # create the branch
                repo_name = "sample_project_etl_bi"
                branch_name = self.run_parameters["branch_name"]
                logger.info("Creating branch {}".format(branch_name))
                try:
                    version_controller.create_branch(repo_name=repo_name, branch_name=branch_name, type="data")
                    logger.info("Created branch")
                except CreateBranchException:
                    logger.info("Branch already exists")

                # push the data
                description = "Sample data"
                if "input_data_description" in self.run_parameters:
                    description = self.run_parameters["input_data_description"]
                commit_id, remote_path = version_controller.push_dataset(repo_name=repo_name,
                                                                         branch_name=branch_name,
                                                                         dataset=dataset, description=description, type="data")
                logger.info("Pushed data: commit ID = {}", commit_id)
                status = {
                    "status": {"status": "Pushed data into repository"},
                    "metric": {"Commit ID": commit_id}
                }

                # store commit ID in parameters file
                self.run_parameters["commit_id"] = commit_id
                with open(os.path.join("/data", self.parameters_file), 'w') as json_file:
                    json.dump(self.run_parameters, json_file)
                logger.info("Stored commit ID in parameters file")


        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self, report_status):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    # this component has 3 parameters: run_name, name of JSON file containing input parameters and commit ID
    # in data repository where params are stored
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    data_prep = FetchData(run_name, params_filename, params_commit_id)
    data_prep.start(run_name=sys.argv[1])